/**
 * @file
 * Attaches behaviors for the Druplippy module.
 */

(function ($) {

Drupal.behaviors.druplippy = {
  attach: function (context) {
    $('.druplippy').css('bottom', '500px').animate({'bottom' : '0'});

    $('.druplippy .ui-icon-circle-close').click(function() {
      $('.druplippy').hide();
    });
  }
};


})(jQuery);
