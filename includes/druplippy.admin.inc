<?php

/**
 * @file
 * Druplippy administration.
 */

/**
 * Menu callback: display Druplippy entries.
 */
function druplippy_entries() {
  $result = db_query("SELECT * FROM {druplippy} ORDER BY dsid ASC");
  $rows = array();
  foreach ($result as $entry) {
    $row = array(
      'title' => check_plain($entry->title),
      'path' => check_plain($entry->path),
      'type' => check_plain(druplippy_get_source_or_type($entry)),
    );
    if (!isset($entry->dsid)) {
      $row['edit'] = l('edit', 'admin/config/user-interface/druplippy/entry/' . $entry->uuid);
      $row['delete'] = l('delete', 'admin/config/user-interface/druplippy/entry/' . $entry->uuid . '/delete');
    }
    else {
      $row[] = '';
      $row[] = '';

    }
    $rows[] = $row;
  }
  if (!count($rows)) {
  	return t('You have no Druplippy entries. You can <a href="!add">add</a> an entry.', array('!add' => url('admin/config/user-interface/druplippy/entries/add')));
  }
  $header = array(t('Title'), t('Path'), t('Type'), '', '');
  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Form to add or edit a druplippy entry.
 */
function druplippy_entry_edit($form, &$form_state, $uuid = NULL) {
	if ($uuid) {
		$form_state['druplippy'] = array('uuid' => $uuid);
		$entry = druplippy_entry_load($uuid);
	}
  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#default_value' => isset($entry->title) ? $entry->title : '',
    '#required' => TRUE,
  );
  $form['path'] = array(
    '#title' => t('Path'),
    '#type' => 'textfield',
    '#description' => t('The menu router path to display the entry. For example, admin/content or node/%.'),
    '#default_value' => isset($entry->path) ? $entry->path : '',
    '#required' => TRUE,
  );
  $form['entry'] = array(
    '#title' => t('Entry'),
    '#type' => 'textarea',
    '#default_value' => isset($entry->entry) ? $entry->entry : '',
    '#required' => TRUE,
  );
  $options = array();
  $options[] = t('N/A');
  $options = $options + variable_get('druplippy_types', array());
  $form['type'] = array(
    '#title' =>  t('Type'),
    '#type' => 'radios',
    '#default_value' => isset($entry->type) ? $entry->type : '',
    '#options' => $options,
    '#description' => t('<a href="!add">Add</a> Druplippy types to classify your entries.', array('!add' => url('admin/config/user-interface/druplippy/types/add'))),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function druplippy_entry_edit_submit($form, &$form_state) {
	$entry = array();
	$update = array();
	$values = $form_state['values'];
	if (isset($form_state['druplippy']['uuid'])) {
		$uuid = $form_state['druplippy']['uuid'];
		$update[] = 'uuid';
	}
	else {
		$uuid = druplippy_generate_uuid();
	}
	$entry = array(
		'uuid' => $uuid,
    'title' => $values['title'],
    'path' => $values['path'],
    'type' => $values['type'],
    'entry' => $values['entry'],
	);
	$form_state['redirect'] = 'admin/config/user-interface/druplippy';
	drupal_write_record('druplippy', $entry, $update);
}

function druplippy_entry_delete($form, &$form_state, $uuid) {
  $form_state['druplippy'] = array('uuid' => $uuid);
  return confirm_form($form, t('Are you sure you want to delete this entry?'), 'admin/config/user-interface/druplippy', '', t('Delete'));
}

function druplippy_entry_delete_submit($form, &$form_state) {
  db_delete('druplippy')->condition('uuid', $form_state['druplippy']['uuid'])->execute();
  $form_state['redirect'] = 'admin/config/user-interface/druplippy';
}

function druplippy_sources() {
	$result = db_query("SELECT * FROM {druplippy_source}");
	$rows = array();
	foreach ($result as $source) {
	  $rows[] = array(
      'title' => check_plain($source->title),
	    'url' => check_plain($source->url),
	    'edit' => l('edit', 'admin/config/user-interface/druplippy/source/' . $source->dsid),
      'delete' => l('delete', 'admin/config/user-interface/druplippy/source/' . $source->dsid . '/delete'),
	  );
	}
	if (!count($rows)) {
		return t('You have no Druplippy sources. You can <a href="!add">add</a> a source.', array('!add' => url('admin/config/user-interface/druplippy/sources/add')));
	}
	$header = array(t('Title'), t('URL'), '', '');
	return theme('table', array('header' => $header, 'rows' => $rows));
}

function druplippy_source_edit($form, &$form_state, $dsid = NULL) {
  if ($dsid) {
    $form_state['druplippy'] = array('dsid' => $dsid);
    $source = druplippy_source_load($dsid);
  }
  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => isset($source->title) ? $source->title : '',
  );
	$form['url'] = array(
    '#title' => t('URL'),
    '#type' => 'textfield',
    '#description' => t('URL to a Druplippy source such as http://druplippy.com/druplippy or http://druplippy.com/druplippy/4'),
    '#required' => TRUE,
    '#default_value' => isset($source->url) ? $source->url : '',
	);
	$form['submit'] = array(
	  '#type' => 'submit',
	  '#value' => t('Submit'),
	);

	return $form;
}

function druplippy_source_edit_submit($form, &$form_state) {
  $source = new stdClass();
  $update = array();
  $values = $form_state['values'];
  if (isset($form_state['druplippy']['dsid'])) {
    $source->dsid = $form_state['druplippy']['dsid'];
    $update[] = 'dsid';
  }
  $source->url = $values['url'];
  $source->title = $values['title'];
  $form_state['redirect'] = 'admin/config/user-interface/druplippy/sources';
  drupal_write_record('druplippy_source', $source, $update);
  druplippy_update_entries($source);
}

function druplippy_source_delete($form, &$form_state, $dsid) {
  $form_state['druplippy'] = array('dsid' => $dsid);
  return confirm_form($form, t('Are you sure you want to delete this source?'), 'admin/config/user-interface/druplippy/sources', '', t('Delete'));
}

function druplippy_source_delete_submit($form, &$form_state) {
  db_delete('druplippy_source')->condition('dsid', $form_state['druplippy']['dsid'])->execute();
  $form_state['redirect'] = 'admin/config/user-interface/druplippy/sources';
}

function druplippy_types() {
  $types = variable_get('druplippy_types', array());
  $rows = array();
  foreach ($types as $key => $type) {
    $rows[] = array(
      'name' => check_plain($type),
      'edit' => l('edit', 'admin/config/user-interface/druplippy/type/' . $key),
      'delete' => l('delete', 'admin/config/user-interface/druplippy/type/' . $key . '/delete'),
    );
  }
  if (!count($rows)) {
    return t('You have no Druplippy types. You can <a href="!add">add</a> a type.', array('!add' => url('admin/config/user-interface/druplippy/types/add')));
  }
  $header = array(t('Name'), '', '');
  return theme('table', array('header' => $header, 'rows' => $rows));
}

function druplippy_type_edit($form, &$form_state, $key = NULL) {
  if (isset($key)) {
    $form_state['druplippy'] = array('key' => $key);
    $types = variable_get('druplippy_types', array());
    $type = $types[$key];
  }
  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => isset($type) ? $type : '',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function druplippy_type_edit_submit($form, &$form_state) {
  $types = variable_get('druplippy_types', array());
  if (isset($form_state['druplippy']['key'])) {
    $types[$form_state['druplippy']['key']] = $form_state['values']['name'];
  }
  else {
    if (!count($types)) {
      // Avoid having any 0 type.
      $types[1] = $form_state['values']['name'];
    }
    else {
      $types[] = $form_state['values']['name'];
    }
  }
  variable_set('druplippy_types', $types);
  $form_state['redirect'] = 'admin/config/user-interface/druplippy/types';
}

function druplippy_type_delete($form, &$form_state, $key) {
  $form_state['druplippy'] = array('key' => $key);
  return confirm_form($form, t('Are you sure you want to delete this type?'), 'admin/config/user-interface/druplippy/types', '', t('Delete'));
}

function druplippy_type_delete_submit($form, &$form_state) {
  $types = variable_get('druplippy_types', array());
  unset($types[$form_state['druplippy']['key']]);
  variable_set('druplippy_types', $types);
  $form_state['redirect'] = 'admin/config/user-interface/druplippy/types';
}

function druplippy_settings($form, &$form_state) {
  $options = array('druplippy' => t('Druplippy character'));
  if (module_exists('help')) {
    $options['help'] = t('Core help');
  }
  if (module_exists('block')) {
    $options['block'] = t('Block');
  }
  $form['druplippy_display'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display settings'),
    '#options' => $options,
    '#default_value' => variable_get('druplippy_display', array('druplippy')),
  );
  $intervals = array(
    0 => t('Each cron run'),
    60 => t('Each hour'),
    1440 => t('Once a day'),
    10080 => t('Once a week'),
  );
  $form['druplippy_update_interval'] = array(
    '#type' => 'select',
    '#title' => t('Update interval'),
    '#options' => $intervals,
    '#description' => t('How often should the entries be updated from remote servers?'),
    '#default_value' => variable_get('druplippy_update_interval', array(1440)),
  );
  return system_settings_form($form);
}
